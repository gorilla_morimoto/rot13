"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function isSmall(c) {
    const codePoint = c.codePointAt(0);
    return 'a'.codePointAt(0) <= codePoint && codePoint <= 'z'.codePointAt(0);
}
function isCapital(c) {
    const codePoint = c.codePointAt(0);
    return 'A'.codePointAt(0) <= codePoint && codePoint <= 'Z'.codePointAt(0);
}
function isAlphabet(c) {
    return isCapital(c) || isSmall(c);
}
function core(char, base) {
    const baseCodePoint = base.codePointAt(0);
    return String.fromCodePoint(((char.codePointAt(0) - baseCodePoint + 13) % 26) + baseCodePoint);
}
function rot13Char(c) {
    return core(c, isCapital(c) ? 'A' : 'a');
}
function rot13(s) {
    return [...s].map(c => (isAlphabet(c) ? rot13Char(c) : c)).join('');
}
exports.rot13 = rot13;
